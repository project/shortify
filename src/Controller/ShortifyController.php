<?php

namespace Drupal\shortify\Controller;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\shortify\AdditionalClass\AvailableIcons;
use Drupal\shortify\AdditionalClass\Helpers\ShortcodeHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\Response;

class ShortifyController extends ControllerBase implements ContainerInjectionInterface {
  private array $defaultShortcodes;

  public function __construct() {
    if ($cache = Drupal::cache()->get('$defaultShortcodes')) {
      $this->defaultShortcodes = $cache->data;
    }else {
      $this->defaultShortcodes = ShortcodeHelper::getDefaultShortcodeList();
      Drupal::cache()->set('$defaultShortcodes', $this->defaultShortcodes);
    }
  }

  public function getAvailableIconList(): JsonResponse {
    return new JsonResponse(AvailableIcons::getFontAwesomeIcons());
  }

  public function deleteFile(): JsonResponse {
    if (isset($_POST['id'])) {
      $id = $_POST['id'];

      try {
        $fileStorageHandler = Drupal::entityTypeManager()
          ->getStorage("file");
        $file = $fileStorageHandler->load($id);

        if ($file) {
          $file->delete();
        }
      } catch (EntityStorageException | InvalidPluginDefinitionException | PluginNotFoundException $e) {
        return new JsonResponse(['id' => 0]);
      }

      return new JsonResponse(['id' => $id]);
    }

    return new JsonResponse(['id' => 0]);
  }

  public function getShortcodesFilteredByParent(): Response {
    $parentName = $_POST["parent"] ?? '';
    $defaultList = $this->defaultShortcodes;
    $parentConfig = ShortcodeHelper::getParentConfig($parentName);

    for ($j = count($defaultList) - 1; $j >= 0; $j--) {
      for ($i = count($defaultList[$j]['elements']) - 1; $i >= 0; $i--) {
        if (!is_null($parentConfig) && isset($parentConfig['child']) && count($parentConfig['child']) > 0) {
          $isIn = FALSE;
          foreach ($parentConfig['child'] as $dependency) {
            if ($defaultList[$j]['elements'][$i]['id'] === $dependency) {
              $isIn = TRUE;
              break;
            }
          }
          if (!$isIn) {
            array_splice($defaultList[$j]['elements'], $i, 1);
            continue;
          }
        }
        if (isset($defaultList[$j]['elements'][$i]['dependency']['parent']) && count($defaultList[$j]['elements'][$i]['dependency']['parent']) > 0) {
          $isIn = FALSE;
          foreach ($defaultList[$j]['elements'][$i]['dependency']['parent'] as $dependency) {
            if ($parentName === $dependency) {
              $isIn = TRUE;
              break;
            }
          }
          if (!$isIn) {
            array_splice($defaultList[$j]['elements'], $i, 1);
          }
        }
      }
    }

    return new Response(\GuzzleHttp\json_encode($defaultList));
  }

  public function addFile(): JsonResponse {
    if (isset($_FILES['files']['name'])) {
      $last = $_POST['last'] ?? '';

      if ($last && strlen($last) > 4) {
        $fileId = explode("{#}", $last, 1)[0];

        try {
          $fileStorageHandler = Drupal::entityTypeManager()
            ->getStorage("file");
          $file = $fileStorageHandler->load($fileId);

          if ($file) {
            $file->delete();
          }
        } catch (EntityStorageException | InvalidPluginDefinitionException | PluginNotFoundException $e) {
        }
      }
      $validators = ['file_validate_extensions' => ''];

      $file = file_save_upload('image', $validators, 'public://', FileSystemInterface::EXISTS_RENAME);

      if ($file) {
        Drupal::service('file.usage')
          ->add($file, 'file', 'shortifys', 'file_ajax');
        $filename = $file->getFileUri();

        return new JsonResponse([
          'id' => $file->id(),
          'url' => str_replace($GLOBALS['base_url'], "", file_create_url($filename)),
        ]);
      }
      else {
        return new JsonResponse(['id' => 0]);
      }
    }
    else {
      return new JsonResponse(['id' => 0]);
    }
  }

  public function getShortcodesList(): Response {
    return new Response(\GuzzleHttp\json_encode($this->defaultShortcodes));
  }
}
