<?php

namespace Drupal\shortify\AdditionalClass;

use Drupal;

class SettingsGenerator
{
    public static function basicSettings($token = NULL) : array
    {
        $settings = [
            "elements" => [
                [
                    "tabName" => t("Main config"),
                    "description" => t("Main element config."),
                    "elements" => [],
                ],
                [
                    "tabName" => t("Size"),
                    "description" => t("Define element size."),
                    "elements" => [
                        [
                            "type" => "number",
                            "name" => t("padding left"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "number",
                            "name" => t("padding right"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "number",
                            "name" => t("padding top"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "number",
                            "name" => t("padding bottom"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "text",
                            "name" => t("margin left"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "text",
                            "name" => t("margin right"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "text",
                            "name" => t("margin top"),
                            "width" => "25",
                            "value" => "",
                        ],
                        [
                            "type" => "text",
                            "name" => t("margin bottom"),
                            "width" => "25",
                            "value" => "",
                        ],
                    ],
                ],
                [
                    "tabName" => t("Customize"),
                    "description" => t("Define additional class, id, styles and others."),
                    "elements" => [
                        [
                            "type" => "text",
                            "name" => t("additional class"),
                            "width" => "50",
                            "value" => "",
                        ],
                        [
                            "type" => "text",
                            "name" => t("element id"),
                            "width" => "50",
                            "value" => "",
                        ],
                        [
                            "type" => "colorpicker",
                            "name" => t("background color"),
                            "atr_name" => "background_color",
                            "width" => "50",
                            "value" => "",
                        ],
                        [
                            "type" => "textarea",
                            "name" => t("container styles"),
                            "atr_name" => "additional_styles",
                            "width" => "50",
                            "value" => "",
                        ],
                    ],
                ],
                [
                    "tabName" => t("Animations"),
                    "description" => t("Specify if, and how, an object should animate. You have a choice of several animations and its parameters. Animation determines how the object will appear or behave."),
                    "elements" => [
                        [
                            "type" => "select",
                            "atr_name" => "animation_name",
                            "name" => t("Animation"),
                            "width" => "50",
                            "select_type" => "list",
                            "select_list" =>  AvailableAnimations::getAnimationList(),
                            "value" => "none",
                        ],
                        [
                            "type" => "number",
                            "atr_name" => "animation_duration",
                            "name" => t("Animation duration (ms)"),
                            "width" => "25",
                            "value" => "500",
                        ],
                        [
                            "type" => "number",
                            "atr_name" => "animation_delay",
                            "name" => t("Animation delay (ms)"),
                            "width" => "25",
                            "value" => "0",
                        ],
                        [
                            "type" => "select",
                            "atr_name" => "hover_class",
                            "name" => t("Effect on hover"),
                            "width" => "50",
                            "select_type" => "list",
                            "select_list" => AvailableAnimations::getHoverList(),
                            "value" => "",
                        ],
                    ],
                ],
                [
                    "tabName" => t("Others"),
                    "description" => t("Additional configuration"),
                    "elements" => [
                        [
                            "type" => "checkbox",
                            "atr_name" => "ps_cache",
                            "name" => t("Should the item be cached?"),
                            "width" => "100",
                            "value" => "false",
                        ],
                        [
                            "type" => "checkbox",
                            "atr_name" => "is_shadow",
                            "name" => t("Drop shadow?"),
                            "width" => "25",
                            "value" => "false",
                        ],
                    ],
                ],
            ],
        ];

        if (!is_null($token)) {
            $shortcodeService = Drupal::service('shortcode');
            $shortcodes = $shortcodeService->loadShortcodePlugins();
            $settings["elements"][0]["elements"] = $shortcodes[$token]["settings"];
        }

        return $settings;
    }
}