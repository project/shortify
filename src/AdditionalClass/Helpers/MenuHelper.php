<?php

namespace Drupal\shortify\AdditionalClass\Helpers;


use Drupal;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\node\NodeInterface;

class MenuHelper {

  public static function getMenuFooter(string $name, string $break = ''): string {
    $links = "";
    $menuTree = Drupal::menuTree();
    $parameters = $menuTree->getCurrentRouteMenuTreeParameters($name);
    $tree = $menuTree->load($name, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menuTree->transform($tree, $manipulators);
    $menuTmp = $menuTree->build($tree);

    $menu = [];
    if (array_key_exists('#items', $menuTmp)) {
      foreach ($menuTmp['#items'] as $item) {
        if ($item['url']->getRouteName() === '<front>') {
          $menu[] = [$item['title'] => '/'];
        } else if ($item['url']->getRouteName() === '<nolink>') {
          $menu[] = [$item['title'] => NULL];
        }
        elseif ($item['url']->getRouteName() == 'entity.node.canonical') {
          $node = [$item['title'] => $item['url']->getRouteParameters()];
          $node = $node[$item['title']];
          $nodeAlias = '';
          foreach ($node as $node_number) {
            $path = '/node/' . (int) $node_number;
            $langCode = Drupal::languageManager()
              ->getCurrentLanguage()
              ->getId();
            $nodeAlias = Drupal::service('path_alias.manager')
              ->getAliasByPath($path, $langCode);
          }
          $menu[] = [$item['title'] => trim($nodeAlias, '/')];
        }
        else {
          $menu[] = [$item['title'] => $item['url']->getInternalPath()];
        }
      }
      $i = 1;

      foreach ($menu as $menu_item) {
        foreach ($menu_item as $name => $url) {
          if (is_null($url)) {
            $menuItem =  "<li><a title='no link' href='#' class='nolink'>$name</a></li>";
          } else {
            $menuItem = "<li><a href='/$url'>$name</a></li>";
          }


          if ($i != count($menu_item)) {
            $links .= "$menuItem$break";
            $i++;
          }
          else {
            $links .= $menuItem;
          }
        }
      }
    }
    else {
      $links = "";
    }

    return $links;
  }

  public static function getMenu($name, $dropdown = FALSE, $styled = FALSE): string {
    $active = "";
    $links = "";
    $dropdownMenu = "";
    $menuTree = Drupal::menuTree();
    $parameters = new MenuTreeParameters();
    $parameters->onlyEnabledLinks();
    $tree = $menuTree->load($name, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menuTree->transform($tree, $manipulators);
    $menu_tmp = $menuTree->build($tree);
    $menu = [];

    if (isset($menu_tmp['#items'])) {
      foreach ($menu_tmp['#items'] as $item) {
        if ($item['url']->isRouted()) {
          if ($item['url']->getRouteName() === '<front>') {
            $menu_item = [
              "url" => '/',
              "name" => $item['title'],
            ];
          }
          elseif ($item['url']->getRouteName() === '<nolink>') {
            $menu_item = [
              "url" => NULL,
              "name" => $item['title'],
            ];
          }
          elseif ($item['url']->getRouteName() == 'entity.node.canonical') {
            $node = [$item['title'] => $item['url']->getRouteParameters()];
            $node = $node[$item['title']];
            $node_alias = '';
            foreach ($node as $node_number) {
              $path = '/node/' . (int) $node_number;
              $langCode = Drupal::languageManager()
                ->getCurrentLanguage()
                ->getId();
              $node_alias = Drupal::service('path_alias.manager')
                ->getAliasByPath($path, $langCode);
            }
            $menu_item = [
              "url" => $node_alias,
              "name" => $item['title'],
            ];
          }
          else {
            $menu_item = [
              "url" => '/' . $item['url']->getInternalPath(),
              "name" => $item['title'],
            ];
          }
        }
        elseif ($item['url']->isExternal()) {
          $menu_item = [
            "url" => $item['url']->getUri(),
            "name" => $item['title'],
          ];
        }
        else {
          $menu_item = [
            "url" => str_replace('base:', '', '/' . $item['url']->toUriString()),
            "name" => $item['title'],
          ];
        }

        if ($dropdown) {
          if (is_array($item['below']) && !empty($item['below'])) {
            if ($styled) {
              $dropdownMenu .= self::getDropdownMenu($item['below'], 1, TRUE);
            }
            else {
              $dropdownMenu .= self::getDropdownMenu($item['below'], 1, FALSE);
            }
            $menu_item['dropdown'] = $dropdownMenu;
            $dropdownMenu = "";
          }
        }

        $menu[] = $menu_item;
      }
    }

    $result = self::getActiveClass();
    $first = FALSE;

    foreach ($menu as $menu_item) {
      $dropdownMenu = $menu_item["dropdown"] ?? "";
      $isUrlNull = is_null($menu_item['url']);

      if ($result && !$first) {
        if (!$isUrlNull && $menu_item['url'] == $result || $menu_item['url'] == trim($result, "/") || $menu_item['name'] == $result) {
          $active = "class='ps-active'";
          $first = TRUE;
        }
      }


      if ($styled && !$isUrlNull) {
        $links .= "<li $active><a href='{$menu_item['url']}'>{$menu_item['name']}</a>$dropdownMenu</li>";
      }
      elseif ($isUrlNull) {
        $links .= "<li $active><a href='#' title='no link' class='nolink'>{$menu_item['name']}</a>$dropdownMenu</li>";
      }
      else {
        $links .= "<li><a href='{$menu_item['url']}'>{$menu_item['name']}</a>$dropdownMenu</li>";
      }
      $active = "";
    }

    return $links;
  }

  private static function getDropdownMenu($dropdown, $level, $styled): string {
    $content_below = "";

    if ($level > 1) {
      $content = $styled ? "<ul class='ps-dropdown'>" : "<ul>";
    }
    else {
      $content = $styled ? "<i class='fas fa-caret-down'></i><ul class='ps-dropdown'>" : "<ul>";
    }

    foreach ($dropdown as $item) {
      $title = $item['title'];

      if ($item['url']->isRouted() && $item['url']->getRouteName() == 'entity.node.canonical') {
        $node = [$item['title'] => $item['url']->getRouteParameters()];
        $node = $node[$item['title']];
        $node_alias = '';

        foreach ($node as $node_number) {
          $path = '/node/' . (int) $node_number;
          $langCode = Drupal::languageManager()
            ->getCurrentLanguage()
            ->getId();
          $node_alias = Drupal::service('path_alias.manager')
            ->getAliasByPath($path, $langCode);
        }
      }
      elseif ($item['url']->isExternal()) {
        $node_alias = $item['url']->getUri();
      }
      elseif ($item['url']->getRouteName() == '<front>') {
        $node_alias = '/';
      }
      elseif ($item['url']->getRouteName() === '<nolink>') {
        $node_alias = NULL;
      }
      else {
        $node_alias = str_replace('base:', '', $item['url']->toUriString());
      }

      $url = $node_alias;
      $isUrlNull = is_null($url);
      $right_arrow = "";

      if (is_array($item['below']) && !empty($item['below'])) {
        $content_below .= self::getDropdownMenu($item['below'], 2, $styled);
        $right_arrow = "<i class='fas fa-caret-right'></i>";
      }

      if ($styled && !$isUrlNull) {
        $content .= "<li><a href='$url'>$title</a>$right_arrow $content_below</li>";
      }
      elseif ($isUrlNull) {
        $content .= "<li><a href='#' title='no link' class='nolink'>$title</a>$right_arrow $content_below</li>";
      }
      else {
        $content .= "<li><a href='$url'>$title</a>$content_below</li>";
      }

      $content_below = "";
    }

    $content .= "</ul>";
    return $content;
  }

  private static function getActiveClass() {
    if (Drupal::service('path.matcher')->isFrontPage()) {
      return "/";
    }

    $node_id = "";
    $parentInfo = NULL;
    $node = Drupal::routeMatch()->getParameter('node');

    if ($node instanceof NodeInterface) {
      $node_id = $node->id();
    }

    $menu_link_manager = Drupal::service('plugin.manager.menu.link');
    $result = $menu_link_manager->loadLinksByRoute('entity.node.canonical', ['node' => $node_id]);

    if (empty($result)) {
      return FALSE;
    }

    foreach ($result as $child) {
      $currentPluginId = $child->getPluginId();
      $getParents = $menu_link_manager->getParentIds($currentPluginId);
      $parent = array_reverse($getParents);
      $parent = reset($parent);
      $parentInfo = $menu_link_manager->getDefinition($parent);
    }

    if (isset($parentInfo['title'])) {
      return $parentInfo['title'];
    }
    if (count($parentInfo['route_parameters']) <= 0 && $parentInfo['route_name'] == "<front>") {
      return "/";
    }

    $path = '/node/' . (int) $parentInfo['route_parameters']['node'];
    $langCode = Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();

    return Drupal::service('path_alias.manager')
      ->getAliasByPath($path, $langCode);
  }

}
