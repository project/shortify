<?php

namespace Drupal\shortify\AdditionalClass\Helpers;

class AttributeHelper
{
    public static function isTrue($atr): bool{
        return isset($atr) && ($atr === 'true' || $atr === true || $atr === 'True');
    }

    public static function stringNotNull($str): bool{
        return isset($str) && strlen($str) > 0 && $str !== "none" && $str !== "null";
    }

    public static function escapeValue($str): string{
        $newStr = trim($str);
        $newStr = str_replace(" ", "_", $newStr);
        $newStr = str_replace("\"", "", $newStr);
        return str_replace("'", "", $newStr);
    }

    public static function escapeClassValue($str): string{
        $newStr = trim($str);
        $newStr = str_replace("  ", " ", $newStr);
        $newStr = str_replace("\"", "", $newStr);
        return str_replace("'", "", $newStr);
    }

    public static function escapeStyleValue($str): string{
        $newStr = trim($str);
        $newStr = str_replace("'", "", $newStr);
        return str_replace("\"", "", $newStr);
    }

}