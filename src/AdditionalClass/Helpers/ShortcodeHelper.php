<?php

namespace Drupal\shortify\AdditionalClass\Helpers;

use Drupal;
use Drupal\block\Entity\Block;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\shortify\AdditionalClass\SettingsGenerator;
use Drupal\views\Entity\View;
use Drupal\webform\Entity\Webform;

class ShortcodeHelper
{
    use StringTranslationTrait;

    public static function getParentConfig($parentName)
    {
        $shortcodeService = Drupal::service('shortcode');
        $shortcodes = $shortcodeService->loadShortcodePlugins();

        foreach ($shortcodes as $short)
            if ($short['id'] === $parentName)
                return $short["dependency"] ?? NULL;

        return NULL;
    }

    public static function getDefaultShortcodeList() : array
    {
        $shortcodeService = Drupal::service('shortcode');
        $shortcodes = $shortcodeService->loadShortcodePlugins();
        $dynamicObj = (new ShortcodeHelper);

        $returnedArray = [
            ["name" => $dynamicObj->t("Layout elements"), "elements" => []],
            ["name" => $dynamicObj->t("Layout builder elements"), "elements" => []],
            ["name" => $dynamicObj->t("Renderable elements"), "elements" => []],
            ["name" => $dynamicObj->t("Additional"), "elements" => []],
        ];

        $additionalIndex = 3;

        foreach ($shortcodes as &$short) {
            $currentSettings = $short["settings"];
            self::completeSettings($currentSettings);
            $short["settings"] = SettingsGenerator::basicSettings()["elements"];
            $short["settings"][0]["elements"] = $currentSettings;

            if (!isset($short["group"]) || !isset($returnedArray[$short["group"]])) {
                $short["group"] = $additionalIndex;
            }

            array_push($returnedArray[$short["group"]]["elements"], $short);
        }

        return $returnedArray;
    }

    private static function completeSettings(&$settings)
    {
        foreach ($settings as &$element) {
            if ($element["type"] === "select" && $element["select_type"] === "server") {
                switch ($element["select_server"]) {
                    case 'menu':
                        $element["select_list"] = menu_ui_get_menus();
                        break;
                    case 'view':
                        $element["select_list"] = self::getViews();
                        break;
                    case 'form':
                        $element["select_list"] = self::getForms();
                        break;
                    case 'block':
                        $element["select_list"] = self::getBlocks();
                        break;
                }
            }
        }
    }

    private static function getBlocks() : array
    {
        $allBlocks = Block::loadMultiple();
        $selectBlocks = [];

        foreach ($allBlocks as $bid => $value) {
            $selectBlocks[$bid] = $value->label();
        }

        return $selectBlocks;
    }

    private static function getForms() : array
    {
        $allForms = Webform::loadMultiple();
        $selectForms = [];

        foreach ($allForms as $name => $options) {
            $selectForms[$name] = $name;
        }

        return $selectForms;
    }

    private static function getViews() : array
    {
        $allViews = View::loadMultiple();
        $selectViews = [];

        foreach ($allViews as $view) {
            $id = $view->id();
            foreach ($view->get('display') as $displayId => $displayView) {
                if ($displayId == 'default') {
                    $selectViews[$id] = $view->label();
                } else {
                    $selectViews[$id . ":" . $displayId] = AttributeHelper::escapeStyleValue( $view->label() . ': ' . $displayView['display_title']);
                }
            }
        }
        return $selectViews;
    }
}