<?php

namespace Drupal\shortify\AdditionalClass\Helpers;


use Drupal\Core\StringTranslation\StringTranslationTrait;

class UrlHelper
{
    use StringTranslationTrait;

    public static function getUrlFromFile(string $file): string
    {
        $url = explode("{#}", $file);
        return $url[1] ?? (new UrlHelper)->t('error with this url');
    }

    public static function getUrlListFromFileListValue(?string $fileListValue): array
    {
        if (!isset($fileListValue) && is_null($fileListValue)) return [];

        $returnedArray = [];
        $listOfUrl = explode("{|#|}", $fileListValue);

        foreach ($listOfUrl as $url) {
            if (strlen($url) > 3) $returnedArray[] = self::getUrlFromFile($url);
        }

        return $returnedArray;
    }
}
