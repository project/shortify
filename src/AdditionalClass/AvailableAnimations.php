<?php

namespace Drupal\shortify\AdditionalClass;

class AvailableAnimations {

  public static function getAnimationList(): array {
    return [
      "none" => t("Animation off"),
      "animate__bounce" => "bounce",
      "animate__flash" => "flash",
      "animate__pulse" => "pulse",
      "animate__rubberBand" => "rubberBand",
      "animate__shakeX" => "shakeX",
      "animate__shakeY" => "shakeY",
      "animate__headShake" => "headShake",
      "animate__swing" => "swing",
      "animate__tada" => "tada",
      "animate__wobble" => "wobble",
      "animate__jello" => "jello",
      "animate__heartBeat" => "heartBeat",
      "animate__backInDown" => "backInDown",
      "animate__backInLeft" => "backInLeft",
      "animate__backInRight" => "backInRight",
      "animate__backInUp" => "backInUp",
      "animate__backOutDown" => "backOutDown",
      "animate__backOutLeft" => "backOutLeft",
      "animate__backOutRight" => "backOutRight",
      "animate__backOutUp" => "backOutUp",
      "animate__bounceIn" => "bounceIn",
      "animate__bounceInDown" => "bounceInDown",
      "animate__bounceInLeft" => "bounceInLeft",
      "animate__bounceInRight" => "bounceInRight",
      "animate__bounceInUp" => "bounceInUp",
      "animate__bounceOut" => "bounceOut",
      "animate__bounceOutDown" => "bounceOutDown",
      "animate__bounceOutLeft" => "bounceOutLeft",
      "animate__bounceOutRight" => "bounceOutRight",
      "animate__bounceOutUp" => "bounceOutUp",
      "animate__fadeIn" => "fadeIn",
      "animate__fadeInDown" => "fadeInDown",
      "animate__fadeInDownBig" => "fadeInDownBig",
      "animate__fadeInLeft" => "fadeInLeft",
      "animate__fadeInLeftBig" => "fadeInLeftBig",
      "animate__fadeInRight" => "fadeInRight",
      "animate__fadeInRightBig" => "fadeInRightBig",
      "animate__fadeInUp" => "fadeInUp",
      "animate__fadeInUpBig" => "fadeInUpBig",
      "animate__fadeInTopLeft" => "fadeInTopLeft",
      "animate__fadeInTopRight" => "fadeInTopRight",
      "animate__fadeInBottomLeft" => "fadeInBottomLeft",
      "animate__fadeInBottomRight" => "fadeInBottomRight",
      "animate__fadeOut" => "fadeOut",
      "animate__fadeOutDown" => "fadeOutDown",
      "animate__fadeOutDownBig" => "fadeOutDownBig",
      "animate__fadeOutLeft" => "fadeOutLeft",
      "animate__fadeOutLeftBig" => "fadeOutLeftBig",
      "animate__fadeOutRight" => "fadeOutRight",
      "animate__fadeOutRightBig" => "fadeOutRightBig",
      "animate__fadeOutUp" => "fadeOutUp",
      "animate__fadeOutUpBig" => "fadeOutUpBig",
      "animate__fadeOutTopLeft" => "fadeOutTopLeft",
      "animate__fadeOutTopRight" => "fadeOutTopRight",
      "animate__fadeOutBottomRight" => "fadeOutBottomRight",
      "animate__fadeOutBottomLeft" => "fadeOutBottomLeft",
      "animate__flip" => "flip",
      "animate__flipInX" => "flipInX",
      "animate__flipInY" => "flipInY",
      "animate__flipOutX" => "flipOutX",
      "animate__flipOutY" => "flipOutY",
      "animate__lightSpeedInRight" => "lightSpeedInRight",
      "animate__lightSpeedInLeft" => "lightSpeedInLeft",
      "animate__lightSpeedOutRight" => "lightSpeedOutRight",
      "animate__lightSpeedOutLeft" => "lightSpeedOutLeft",
      "animate__rotateIn" => "rotateIn",
      "animate__rotateInDownLeft" => "rotateInDownLeft",
      "animate__rotateInDownRight" => "rotateInDownRight",
      "animate__rotateInUpLeft" => "rotateInUpLeft",
      "animate__rotateInUpRight" => "rotateInUpRight",
      "animate__rotateOut" => "rotateOut",
      "animate__rotateOutDownLeft" => "rotateOutDownLeft",
      "animate__rotateOutDownRight" => "rotateOutDownRight",
      "animate__rotateOutUpLeft" => "rotateOutUpLeft",
      "animate__rotateOutUpRight" => "rotateOutUpRight",
      "animate__hinge" => "hinge",
      "animate__jackInTheBox" => "jackInTheBox",
      "animate__rollIn" => "rollIn",
      "animate__rollOut" => "rollOut",
      "animate__zoomIn" => "zoomIn",
      "animate__zoomInDown" => "zoomInDown",
      "animate__zoomInLeft" => "zoomInLeft",
      "animate__zoomInRight" => "zoomInRight",
      "animate__zoomInUp" => "zoomInUp",
      "animate__zoomOut" => "zoomOut",
      "animate__zoomOutDown" => "zoomOutDown",
      "animate__zoomOutLeft" => "zoomOutLeft",
      "animate__zoomOutRight" => "zoomOutRight",
      "animate__zoomOutUp" => "zoomOutUp",
      "animate__slideInDown" => "slideInDown",
      "animate__slideInLeft" => "slideInLeft",
      "animate__slideInRight" => "slideInRight",
      "animate__slideInUp" => "slideInUp",
      "animate__slideOutDown" => "slideOutDown",
      "animate__slideOutLeft" => "slideOutLeft",
      "animate__slideOutRight" => "slideOutRight",
      "animate__slideOutUp" => "slideOutUp",
    ];
  }

  public static function getHoverList(): array {
    return [
      "" => t("none"),
      "hvr-grow" => "hvr-grow",
      "hvr-shrink" => "hvr-shrink",
      "hvr-pulse" => "hvr-pulse",
      "hvr-pulse-grow" => "hvr-pulse-grow",
      "hvr-pulse-shrink" => "hvr-pulse-shrink",
      "hvr-push" => "hvr-push",
      "hvr-pop" => "hvr-pop",
      "hvr-bounce-in" => "hvr-bounce-in",
      "hvr-bounce-out" => "hvr-bounce-out",
      "hvr-rotate" => "hvr-rotate",
      "hvr-grow-rotate" => "hvr-grow-rotate",
      "hvr-float" => "hvr-float",
      "hvr-sink" => "hvr-sink",
      "hvr-bob" => "hvr-bob",
      "hvr-hang" => "hvr-hang",
      "hvr-skew" => "hvr-skew",
      "hvr-skew-forward" => "hvr-skew-forward",
      "hvr-skew-backward" => "hvr-skew-backward",
      "hvr-wobble-horizontal" => "hvr-wobble-horizontal",
      "hvr-wobble-vertical" => "hvr-wobble-vertical",
      "hvr-wobble-to-bottom-right" => "hvr-wobble-to-bottom-right",
      "hvr-wobble-to-top-right" => "hvr-wobble-to-top-right",
      "hvr-wobble-top" => "hvr-wobble-top",
      "hvr-wobble-bottom" => "hvr-wobble-bottom",
      "hvr-wobble-skew" => "hvr-wobble-skew",
      "hvr-buzz" => "hvr-buzz",
      "hvr-buzz-out" => "hvr-buzz-out",
      "hvr-forward" => "hvr-forward",
      "hvr-backward" => "hvr-backward",
      "hvr-shadow" => "hvr-shadow",
      "hvr-grow-shadow" => "hvr-grow-shadow",
      "hvr-float-shadow" => "hvr-float-shadow",
      "hvr-glow" => "hvr-glow",
      "hvr-shadow-radial" => "hvr-shadow-radial",
      "hvr-box-shadow-outset" => "hvr-box-shadow-outset",
      "hvr-box-shadow-inset" => "hvr-box-shadow-inset",
    ];
  }

}


































































































