<?php

namespace Drupal\shortify\AdditionalClass;

use Drupal\Core\Language\LanguageInterface;
use Drupal\shortcode\Plugin\ShortcodeBase;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;

abstract class PsShortcodeBase extends ShortcodeBase
{
    private array $attr;
    private string $content;

    abstract protected function buildElement(): string;

    private function getDefaultAttributes(): array {
        return [
            'id' => '',
            'class' => '',
            'style' => '',
            'animation_name' => '',
            'is_shadow' => '',
            'animation_delay' => '',
            'animation_duration' => '',
            'hover_class' => '',
            'additional_styles' => 'position:relative;',
        ];
    }

    private function resetDefaultAttribute(){
        foreach ($this->getDefaultAttributes() as $name => $default) {
            if (!array_key_exists($name, $this->attr)) {
                $this->attr[$name] = $default;
            }
        }
    }

    private function extendDefaultAttributes($attributes)
    {
        if (!is_array($attributes)) $attributes = [];

        foreach ($this->getDefaultAttributes() as $name => $default) {
            if (!array_key_exists($name, $attributes)) {
                $attributes[$name] = $default;
            }
        }

        if (is_array($attributes['class'])) $attributes['class'] = implode(" ", $attributes['class']);

        return $attributes;
    }

    protected function renderShortcode($shortcodeHtml, $full = FALSE, $addRenderClass = TRUE): string
    {
        $container = '';
        $animationData = '';
        $renderClass = $addRenderClass ? "ps-shortcode-render-container" : "";

        $this->addDefStyle($this->attr['additional_styles']);
        $this->addDefClass($this->attr['hover_class']);

        if ($full) $this->addDefStyle("width: 100%;");

        if (AttributeHelper::isTrue($this->attr['is_shadow']))
            $this->addDefStyle("filter: drop-shadow(5px 4px 3px #00000061);");

        if (AttributeHelper::stringNotNull($this->attr['animation_name'])) {
            $this->addDefClass("wow " . $this->attr['animation_name']);

            if (AttributeHelper::stringNotNull($this->attr['animation_delay'])) {
                $delay = ((int)$this->attr['animation_delay']) / 1000.0;
                $animationData = "data-wow-delay=\"{$delay}s\"";
            }

            if (AttributeHelper::stringNotNull($this->attr['animation_duration'])) {
                $duration = ((int)$this->attr['animation_duration']) / 1000.0;
                $animationData .= " data-wow-duration=\"{$duration}s\"";
            }
        }

        $this->attr['id'] = AttributeHelper::stringNotNull($this->attr['id'])
            ? 'id="' . AttributeHelper::escapeValue($this->attr['id']) . '"'
            : '';

        $this->attr['class'] = AttributeHelper::stringNotNull($this->attr['class'])
            ? 'class="' . $renderClass . ' ' . $this->attr['class'] . '"'
            : 'class="' . $renderClass . '"';

        $this->attr['style'] = AttributeHelper::stringNotNull($this->attr['style'])
            ? 'style="' . $this->attr['style'] . '"'
            : '';

        $container .= "<div {$this->attr['id']} {$this->attr['class']} {$this->attr['style']} $animationData>";
        $container .= $shortcodeHtml;
        $container .= '</div>';

        $this->resetDefaultAttribute();
        return $container;
    }

    protected function addDefClass($def)
    {
        if (!AttributeHelper::stringNotNull($def)) return;

        $this->attr['class'] .= " " . $def;
        $this->attr['class'] = AttributeHelper::escapeClassValue($this->attr['class']);
    }

    protected function addDefStyle($style)
    {
        if (!AttributeHelper::stringNotNull($style)) return;

        $last = substr(trim($this->attr['style']), -1);
        $this->attr['style'] .= $last === ';'
            ? " " . $style
            : "; " . $style;

        $this->attr['style'] = AttributeHelper::escapeStyleValue($this->attr['style']);
    }

    protected function getSettings(string $key, string $defaultValue = ''): string{
        return $this->attr[$key] ?? $defaultValue;
    }

    protected function getContent(): string{
        return $this->content ?? '';
    }

    public function process(array $attributes, $text, $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string
    {
        $this->attr = $this->extendDefaultAttributes(array_filter($attributes));
        $this->content = $text;
        return $this->buildElement();
    }

    public function tips($long = FALSE): string
    {
        return '<p>' . $this->t("Shortify") . '</p>';
    }
}
