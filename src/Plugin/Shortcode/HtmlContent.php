<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_html",
 *   title = @Translation("HTML content"),
 *   description = @Translation("Create element with html content in"),
 *   group = "1",
 *   settings = {
 *      {
 *         "type" = "html",
 *         "atr_name" = "html_content",
 *         "name" = @Translation("Html content"),
 *         "width" = "100",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class HtmlContent extends PsShortcodeBase
{
    public function buildElement():string
    {
        $render = $this->getSettings('html_content');
        $this->addDefClass('ps-html-content');

        return $this->renderShortcode($render);
    }
}
