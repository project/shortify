<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_container",
 *   title = @Translation("Container"),
 *   description = @Translation("Create a container"),
 *   group = "1",
 *   settings = {
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_fluid",
 *         "name" = @Translation("Use full page width?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_paralax",
 *         "name" = @Translation("Use paralax background?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "file",
 *         "atr_name" = "paralax_file",
 *         "name" = @Translation("Paralax file"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_filter",
 *         "name" = @Translation("Use paralax filter?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "filter_color",
 *         "name" = @Translation("Filter color"),
 *         "width" = "50",
 *         "value" = "#ffffff"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "filter_opacity",
 *         "name" = @Translation("Filter opacity"),
 *         "width" = "50",
 *         "value" = "50"
 *      }
 *   }
 *
 * )
 */
class Container extends PsShortcodeBase
{

    public function buildElement(): string
    {

        $class = "container";
        $content = $this->getContent();
        $filter = AttributeHelper::isTrue($this->getSettings('is_filter'));
        $filterColor = $this->getSettings('filter_color', "#ffffff");
        $filterOpacity = ((int)$this->getSettings('filter_opacity', '50')) / 100.0;
        $paralaxFile = $this->getSettings('paralax_file');
        $paralax = AttributeHelper::isTrue($this->getSettings('is_paralax'));
        $fluid = AttributeHelper::isTrue($this->getSettings('is_fluid'));
        $file = AttributeHelper::stringNotNull($paralaxFile) ? UrlHelper::getUrlFromFile($paralaxFile) : "";

        if ($fluid) $class = "container-fluid";

        if ($filter) $content = "<div class='ps-paralax-filter' style='background: $filterColor; opacity: $filterOpacity'></div>" . $content;

        if ($paralax && AttributeHelper::stringNotNull($file)) {
            $this->addDefStyle("background-image: url('$file');");
            $this->addDefClass("ps-paralax");
        }

        $this->addDefClass($class);

        return $this->renderShortcode($content, FALSE, FALSE);
    }
}
