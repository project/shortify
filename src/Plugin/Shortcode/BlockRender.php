<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\block\Entity\Block;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_blockrender",
 *   title = @Translation("Block"),
 *   description = @Translation("Render selected block"),
 *   group = "2",
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "block_name",
 *         "name" = @Translation("Blok"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "block",
 *         "value" = "default"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class BlockRender extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $blockName = $this->getSettings('block_name');
        $renderBlock = "";

        if (AttributeHelper::stringNotNull($blockName)) {
            $block = Block::load($blockName);
            $block_content = Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
            $renderBlock = Drupal::service('renderer')->renderRoot($block_content);
        }

        return $this->renderShortcode($renderBlock);
    }
}
