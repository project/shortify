<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic video shortcode
 *
 * @Shortcode(
 *   id = "ps_video",
 *   title = @Translation("Video"),
 *   description = @Translation("Create video element."),
 *   settings = {
 *      {
 *         "type" = "file_list",
 *         "accept" = "video/*",
 *         "atr_name" = "video_list_src",
 *         "name" = @Translation("Select one or more video"),
 *         "width" = "100",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "video_fit",
 *         "name" = @Translation("Video fit"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "fill" = @Translation("fill"),
 *              "contain" = @Translation("contain"),
 *              "cover" = @Translation("cover"),
 *              "none" = @Translation("none")
 *         },
 *         "value" = "cover"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "video_position",
 *         "name" = @Translation("Video position"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "center" = @Translation("center"),
 *              "bottom" = @Translation("bottom"),
 *              "top" = @Translation("top"),
 *              "left" = @Translation("left"),
 *              "right" = @Translation("right")
 *         },
 *         "value" = "center"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "video_radius",
 *         "name" = @Translation("Video border radius"),
 *         "width" = "25",
 *         "value" = "0px"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "video_width",
 *         "name" = @Translation("Video width"),
 *         "width" = "25",
 *         "value" = "100%"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "video_height",
 *         "name" = @Translation("Video height"),
 *         "width" = "25",
 *         "value" = "100%"
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "is_control",
 *         "name" = @Translation("Use video control?"),
 *         "width" = "25",
 *         "value" = "false"
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "is_autoplay",
 *         "name" = @Translation("Autoplay video?"),
 *         "width" = "25",
 *         "value" = "true"
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "is_loop",
 *         "name" = @Translation("Play video loop?"),
 *         "width" = "25",
 *         "value" = "true"
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "is_muted",
 *         "name" = @Translation("Mute Video?"),
 *         "width" = "25",
 *         "value" = "true"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Video extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $fit = $this->getSettings('video_fit', "fill");
        $position = $this->getSettings('video_position', "center");
        $width = $this->getSettings('video_width', "100%");
        $height = $this->getSettings('video_height', "auto");
        $radius = $this->getSettings('video_radius', "0px");
        $source = $this->getSettings('video_list_src');
        $videoList = AttributeHelper::stringNotNull($source) ? UrlHelper::getUrlListFromFileListValue($source) : [];

        $isControl = AttributeHelper::isTrue($this->getSettings('is_control')) ? "controls" : "";
        $isAutoplay = AttributeHelper::isTrue($this->getSettings('is_autoplay')) ? "autoplay" : "";
        $isMuted = AttributeHelper::isTrue($this->getSettings('is_muted')) ? "muted" : "";
        $isLoop = AttributeHelper::isTrue($this->getSettings('is_loop')) ? "loop" : "";

        $additionalStyle = "style='align-self: flex-start; width: $width; height: $height; object-fit: $fit; object-position: $position;'";
        $returnedHtml = '';
        $errorText = t('Your browser does not support the video tag. Download better browser!');

        foreach ($videoList as $image) {
            $this->addDefStyle("display:block; height: 100%;");
            $this->addDefClass('ps-video-in');

            $returnedHtml .= $this->renderShortcode("
                <div style='display: flex; width: $width; margin:auto; height: $height; border-radius: $radius; overflow: hidden'>
                    <video width='$width' height='$height' $isAutoplay $isMuted $isLoop $isControl $additionalStyle>
                        <source src='$image'>
                        $errorText
                    </video>
                </div>", TRUE);
        }

        return $returnedHtml;
    }
}

