<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_row",
 *   title = @Translation("Row"),
 *   description = @Translation("Create a row contains columns"),
 *   group = "1",
 *   dependency = {
 *      "parent" = {},
 *      "child" = { "ps_col" }
 *   },
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "direction",
 *         "name" = @Translation("Row direction"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "d_column" = @Translation("column"),
 *             "d_row" = @Translation("row")
 *         },
 *         "value" = "d_row"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "align_dir",
 *         "name" = @Translation("Aligin items in row"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "center" = @Translation("center"),
 *             "flex-start" = @Translation("flex-start"),
 *             "flex-end" = @Translation("flex-end"),
 *             "stretch" = @Translation("stretch")
 *         },
 *         "value" = "flex-start"
 *      }
 *   }
 * )
 */
class Row extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $alignDir = $this->getSettings('align_dir', "flex-start");
        $direction = $this->getSettings('direction');
        $directionDir = $direction === "d_column" ? "flex-direction: column !important;" : "flex-direction: row !important;";
        $align = "justify-content: $alignDir !important;";

        $this->addDefStyle("$align $directionDir flex-wrap: wrap;");
        $this->addDefClass('row');

        return $this->renderShortcode($this->getContent(), false, false);
    }
}
