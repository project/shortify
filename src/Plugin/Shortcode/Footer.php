<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\MenuHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_footer",
 *   title = @Translation("Footer"),
 *   description = @Translation("Create a page footer"),
 *   group = "0",
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "name",
 *         "name" = @Translation("Footer type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "default" = @Translation("Default")
 *         },
 *         "value" = "default"
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "copyright",
 *         "name" = @Translation("Copyright text"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "navigation",
 *         "name" = @Translation("Main menu"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "menu",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "footer_navigation",
 *         "name" = @Translation("Footer menu"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "menu",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "file",
 *         "atr_name" = "background_img",
 *         "name" = @Translation("Background image"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "file",
 *         "atr_name" = "logo_img",
 *         "name" = @Translation("Footer logo"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Footer extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $backgroundImage = $this->getSettings('background_img');
        $footerLogoImg = $this->getSettings('logo_img');
        $copyright = $this->getSettings('copyright');
        $mainNavigation = $this->getSettings('navigation');
        $footerNavigation = $this->getSettings('footer_navigation');

        $bgFooter = AttributeHelper::stringNotNull($backgroundImage) ? UrlHelper::getUrlFromFile($this->getSettings('background_img')) : '';
        $footerLogo = AttributeHelper::stringNotNull($footerLogoImg) ? UrlHelper::getUrlFromFile($this->getSettings('logo_img')) : '';
        $links = MenuHelper::getMenu($mainNavigation, '', false);
        $footerLinks = MenuHelper::getMenuFooter($footerNavigation);

        $render_footer = "
                <div id='ps-footer-with-menu' style='background-image: url($bgFooter);'>
                    <div class='container'>
                        <div class='ps-footer-logo'>
                            <img alt='Footer logo' src='$footerLogo'/>
                        </div>
                        <div class='ps-menu-links'>
                            <ul class='ps-nav ps-unstyled-list'>" . $links . "</ul>
                        </div>
                    </div>
                </div>
                <div id='ps-postfooter'>
                    <div class='container'>
                        <div class='ps-copyright'><p>$copyright</p></div>
                        <div class='ps-footer-links'>
                            <ul class='ps-nav ps-unstyled-list'>" . $footerLinks . "</ul>
                        </div>
                    </div>
                </div>";

        return $this->renderShortcode($render_footer);
    }
}
