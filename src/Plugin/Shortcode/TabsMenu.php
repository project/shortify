<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_tabs_menu",
 *   title = @Translation("Tabs menu"),
 *   description = @Translation("Create a tabs menu."),
 *   dependency = {
 *      "parent" = {},
 *      "child" = { "ps_tabs_menu_item" }
 *   },
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "name",
 *         "name" = @Translation("Tabs type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "default" = @Translation("Default")
 *         },
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "tabs_header",
 *         "name" = @Translation("Tabs header text"),
 *         "width" = "25",
 *         "value" = "Simple tab menu"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "tabs_description",
 *         "name" = @Translation("Tabs description text"),
 *         "width" = "25",
 *         "value" = ""
 *      }
 *   }
 * )
 */
class TabsMenu extends PsShortcodeBase {

  public function buildElement(): string {

    $type = $this->getSettings('name', "default");
    $tabsHeader = $this->getSettings('tabs_header');
    $tabsDescription = $this->getSettings('tabs_description');

    return $this->renderShortcode("
      <div class='tabs-menu-container tabs-menu-type-$type'>
        <h1 class='tabs-menu-container--header'>$tabsHeader</h1>
        <p class='tabs-menu-container--description'>$tabsDescription</p>
        <div class='tabs-menu-container--tabs'></div>
        <div class='tabs-menu-container--content'>
            {$this->getContent()}
        </div>
      </div>
    ");
  }
}

