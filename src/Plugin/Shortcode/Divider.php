<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_divider",
 *   title = @Translation("Divider"),
 *   description = @Translation("Create divider."),
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "divider_type",
 *         "name" = @Translation("Divider type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = @Translation("Default"),
 *              "v1" = @Translation("Alternative 1"),
 *              "v2" = @Translation("Alternative 2"),
 *         },
 *         "value" = "default"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Divider extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $dividerType = $this->getSettings('divider_type');
        $returnDivider = "";

        switch ($dividerType) {
            case "default":
                $returnDivider = "
                    <hr/>
                ";
                break;
            case "v1":
                $returnDivider = "
                    <hr class='ps-gradient-divider'>
                ";
                break;
            case "v2":
                $returnDivider = "
                    <hr class='ps-gradient-default-divider'>
                ";
                break;
        }

        $this->addDefClass('ps-divider-container');

        return $this->renderShortcode($returnDivider);
    }
}

