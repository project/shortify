<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_col",
 *   title = @Translation("Column"),
 *   description = @Translation("Create a column"),
 *   group = "1",
 *   dependency = {
 *      "parent" = { "ps_row" },
 *      "child" = {}
 *   },
 *   settings = {
 *      {
 *         "type" = "number",
 *         "atr_name" = "sm",
 *         "name" = @Translation("Size sm"),
 *         "width" = "25",
 *         "value" = "12"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "md",
 *         "name" = @Translation("Size md"),
 *         "width" = "25",
 *         "value" = "12"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "lg",
 *         "name" = @Translation("Size lg"),
 *         "width" = "25",
 *         "value" = "12"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "xl",
 *         "name" = @Translation("Size xl"),
 *         "width" = "25",
 *         "value" = "12"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "direction",
 *         "name" = @Translation("Direction"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "d_column" = @Translation("column"),
 *             "d_row" = @Translation("row")
 *         },
 *         "value" = "d_column"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "align_dir",
 *         "name" = @Translation("Aligin items in col"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "center" = @Translation("center"),
 *             "flex-start" = @Translation("flex-start"),
 *             "flex-end" = @Translation("flex-end"),
 *             "stretch" = @Translation("stretch")
 *         },
 *         "value" = "flex-start"
 *      }
 *   }
 * )
 */
class Column extends PsShortcodeBase
{
    public function buildElement(): string
    {
        $class = '';
        $direction = $this->getSettings('direction');
        $alignDir = $this->getSettings('align_dir', "center");
        $sm = $this->getSettings('sm');
        $md = $this->getSettings('md');
        $lg = $this->getSettings('lg');
        $xl = $this->getSettings('xl');

        if (AttributeHelper::stringNotNull($sm))
            $class .= ' col-sm-' . $sm;

        if (AttributeHelper::stringNotNull($md))
            $class .= ' col-md-' . $md;

        if (AttributeHelper::stringNotNull($lg))
            $class .= ' col-lg-' . $lg;

        if (AttributeHelper::stringNotNull($xl))
            $class .= ' col-xl-' . $xl;

        $class .= $direction === "d_column"
            ? " ps-shortcode-col"
            : " ps-shortcode-flex-none";

        $align = "justify-content: $alignDir !important;";

        $this->addDefStyle($align);
        $this->addDefClass($class);

        return $this->renderShortcode($this->getContent(), false, false);
    }
}
