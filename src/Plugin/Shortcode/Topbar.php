<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\Core\Url;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_topbar",
 *   title = @Translation("Topbar"),
 *   description = @Translation("Create a page topbar"),
 *   group = "0",
 *   settings = {
 *      {
 *         "type" = "text",
 *         "atr_name" = "fb_link",
 *         "name" = @Translation("Link facebook"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "twitter_link",
 *         "name" = @Translation("Link twitter"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "tel",
 *         "name" = @Translation("Contact phone"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "email",
 *         "name" = @Translation("Contact email"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_register",
 *         "name" = @Translation("Show login button?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_language",
 *         "name" = @Translation("Show language select?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Topbar extends PsShortcodeBase {

  public function buildElement(): string {
    $html = '';
    $fbLink = $this->getSettings('fb_link');
    $twitterLink = $this->getSettings('twitter_link');
    $tel = $this->getSettings('tel');
    $email = $this->getSettings('email');

    if (AttributeHelper::isTrue($this->getSettings('is_language'))) {
      $html .= $this->buildLanguageSelectorElement();
    }
    if (AttributeHelper::isTrue($this->getSettings('is_register'))) {
      $html .= $this->buildLoginRegisterButton();
    }
    if (AttributeHelper::stringNotNull($fbLink)) {
      $html .= $this->getSocialCode('fb', $fbLink);
    }
    if (AttributeHelper::stringNotNull($twitterLink)) {
      $html .= $this->getSocialCode('twitter', $twitterLink);
    }
    if (AttributeHelper::stringNotNull($tel)) {
      $html .= $this->getContactCode('phone', $tel);
    }
    if (AttributeHelper::stringNotNull($email)) {
      $html .= $this->getContactCode('mail', $email);
    }

    $html = "
          <div class='topbar-container'>
              <div class='container'>
                  <div class='row'>
                      <section class='col-sm'>
                          <div class='region-topbar'>$html</div>
                      </section>
                  </div>
              </div>
          </div>";

    return $this->renderShortcode($html);
  }

  public function buildLanguageSelectorElement(): string {
    $languages = Drupal::languageManager()->getLanguages();
    $current = Drupal::languageManager()->getCurrentLanguage()->getId();
    $flag = "flag flag-" . $current;
    $flag = "<span class='" . $flag . "'></span>";
    $code = " <div class='lang-selector'> <span>" . $flag . t(Drupal::languageManager()
        ->getLanguageName($current)) . "</span><ul>";
    $currentPath = Drupal::request()->getRequestUri();
    $notFoundLangError = t('Not found any languages');

    foreach ($languages as $key => $lang) {
      if ($current !== $key) {
        $newUrl = Url::fromUri("internal:$currentPath", [
          'language' => Drupal::languageManager()->getLanguage($key),
        ]);
        $newPath = $newUrl->toString();
        $flag = "flag flag-" . $key;
        $flag = "<span class='" . $flag . "'></span>";
        $code .= "<li class='$key'><a title='$key' href='$newPath'>" . $flag . t(Drupal::languageManager()
            ->getLanguageName($key)) . "</a></li>";
      }
    }

    if (count($languages) == 1) {
      $code .= "<li class='no-lang'>$notFoundLangError</li>";
    }

    $code .= "</ul></div>";

    return $code;
  }

  public function buildLoginRegisterButton(): string {
    if (Drupal::currentUser()->isAnonymous()) {
      $href = '/user';
      $text = t('Login / Register');
      $icon = 'fas fa-user-lock';
      $title = t('Login, register link');
    }
    else {
      $href = '/user/logout';
      $text = t('Logout');
      $icon = 'fas fa-sign-out-alt';
      $title = t('Logout link');
    }

    return "<a href='$href' title='$title'><i class='$icon'></i><span>$text</span></a>";
  }

  public function getContactCode($type, $content): string {
    $icon = t('wrong type');
    $href = t('wrong type');
    $title = '';

    switch ($type) {
      case 'phone':
        $icon = 'fas fa-phone';
        $href = 'tel:';
        $title = t('Phone contact link');
        break;
      case 'mail':
        $icon = 'fas fa-envelope';
        $href = 'mailto:';
        $title = t('Mail contact link');
        break;
    }

    $href .= $content;

    return "<a class='contact-element' title='$title' href='$href'><i class='$icon'></i><span>" . $content . "</span></a>";
  }

  public function getSocialCode($type, $link): string {
    $icon = t('wrong type');
    $title= '';

    switch ($type) {
      case 'fb':
        $icon = 'fab fa-facebook-f';
        $title= t('Facebook link');
        break;
      case 'twitter':
        $icon = 'fab fa-twitter';
        $title= t('Twitter link');
        break;
      case 'gplus':
        $icon = 'fab fa-google-plus-g';
        $title= t('Google link');
        break;
      case 'youtube':
        $icon = 'fab fa-youtube';
        $title= t('Youtube link');
        break;
      case 'linkedin':
        $icon = 'fab fa-linkedin';
        $title= t('Linkedin link');
        break;
      case 'skype':
        $icon = 'fab fa-skype';
        $title= t('Skype link');
        break;
      case 'rss':
        $icon = 'fas fa-rss';
        $title= t('Rss link');
        break;
    }

    return "<a class='social-icon' title='$title' href='$link'><i class='$icon'></i></a>";
  }
}
