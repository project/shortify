<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\MenuHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_menu",
 *   title = @Translation("Menu"),
 *   description = @Translation("Create menu"),
 *   group = "2",
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "menu",
 *         "name" = @Translation("Menu"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "menu",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Menu extends PsShortcodeBase {

  public function buildElement(): string {
    $menuSettings = $this->getSettings('menu');
    $render_menu = MenuHelper::getMenu($menuSettings, TRUE, FALSE);
    return $this->renderShortcode($render_menu);
  }

}
