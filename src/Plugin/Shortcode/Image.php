<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_image",
 *   title = @Translation("Image"),
 *   description = @Translation("Create image."),
 *   settings = {
 *      {
 *         "type" = "file_list",
 *         "accept" = "image/*",
 *         "atr_name" = "image_list_src",
 *         "name" = @Translation("Select one or more images"),
 *         "width" = "100",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "img_fit",
 *         "name" = @Translation("Image fit"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "fill" = @Translation("fill"),
 *              "contain" = @Translation("contain"),
 *              "cover" = @Translation("cover"),
 *              "none" = @Translation("none")
 *         },
 *         "value" = "cover"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "img_position",
 *         "name" = @Translation("Image position"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "center" = @Translation("center"),
 *              "bottom" = @Translation("bottom"),
 *              "top" = @Translation("top"),
 *              "left" = @Translation("left"),
 *              "right" = @Translation("right")
 *         },
 *         "value" = "center"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "img_radius",
 *         "name" = @Translation("Image border radius"),
 *         "width" = "50",
 *         "value" = "0px"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "img_width",
 *         "name" = @Translation("Image width"),
 *         "width" = "25",
 *         "value" = "100%"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "img_height",
 *         "name" = @Translation("Image height"),
 *         "width" = "25",
 *         "value" = "100%"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "img_group",
 *         "name" = @Translation("Image group (galery etc.)"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "img_alt",
 *         "name" = @Translation("Image alt"),
 *         "width" = "25",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Image extends PsShortcodeBase {


  public function buildElement(): string {
    $fit = $this->getSettings('img_fit', "cover");
    $position = $this->getSettings('img_position', "center");
    $width = $this->getSettings('img_width', "100%");
    $height = $this->getSettings('img_height', "auto");
    $radius = $this->getSettings('img_radius', "0px");
    $alt = $this->getSettings('img_alt');
    $source = $this->getSettings('image_list_src');
    $imgGroup = $this->getSettings('img_group');
    $shouldBeGallery = AttributeHelper::stringNotNull($imgGroup);
    $addFancyBox = $shouldBeGallery ? "data-fancybox='$imgGroup'" : '';
    $imageList = AttributeHelper::stringNotNull($source) ? UrlHelper::getUrlListFromFileListValue($this->getSettings('image_list_src')) : [];


    $additionalStyle = "style='align-self: flex-start; border-radius: $radius; object-fit: $fit; object-position: $position; width: $width; height: $height;'";
    $returnedHtml = '';

    foreach ($imageList as $image) {
      $this->addDefStyle("display:block; height: 100%;");
      $this->addDefClass('ps-image-in');
      $href = $shouldBeGallery ? "href='$image'" : "href='#'";
      $returnedHtml .= $this->renderShortcode("<a 
           $href
            style='display: flex;  margin:auto; width: $width; height: $height;' 
            $addFancyBox
            data-caption='$alt'>
              <img $additionalStyle alt='$alt' src='$image'/>
        </a>", TRUE);
    }

    return $returnedHtml;
  }

}

