<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;
use Drupal\webform\Entity\Webform;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_formrender",
 *   title = @Translation("Form"),
 *   description = @Translation("Render selected form"),
 *   group = "2",
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "form_name",
 *         "name" = @Translation("Form"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "form",
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class FormRender extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $formName = $this->getSettings('form_name');

        if (!AttributeHelper::stringNotNull($formName)) {
            $render = t('Not found selected form.');
        } else {
            $webform = Webform::load($formName);
            $build = Drupal::entityTypeManager()
                ->getViewBuilder('webform')
                ->view($webform);
            $render = render($build);
        }

        return $this->renderShortcode($render);
    }
}
