<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_slider_item",
 *   title = @Translation("Slider item"),
 *   description = @Translation("Create slider item with content. ( this is one slide )"),
 *   dependency = {
 *      "parent" = { "ps_slider" },
 *      "child" = { "ps_html", "ps_image", "ps_video", "ps_counter", "ps_icon", "ps_quote", "ps_block", "ps_form", "ps_view"}
 *   },
 *   settings = {
 *   }
 * )
 */
class SliderItem extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $this->addDefClass('swiper-slide');
        return $this->renderShortcode($this->getContent(), false, false);
    }
}

