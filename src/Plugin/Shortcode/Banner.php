<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_banner",
 *   title = @Translation("Banner"),
 *   description = @Translation("Create a hover animated banner."),
 *   settings = {
 *     {
 *         "type" = "file",
 *         "atr_name" = "background_img",
 *         "name" = @Translation("Background image"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "select",
 *         "atr_name" = "banner_type",
 *         "name" = @Translation("Banner type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = @Translation("Default"),
 *              "v1" = @Translation("Alternative 1"),
 *              "v2" = @Translation("Alternative 2")
 *         },
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "title",
 *         "name" = @Translation("Banner title"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "textarea",
 *         "atr_name" = "description",
 *         "name" = @Translation("Banner description"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "banner_url",
 *         "name" = @Translation("Banner url link"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Banner extends PsShortcodeBase {

  public function buildElement(): string {
    $bgBanner = $this->getSettings('background_img');
    $bgImage = AttributeHelper::stringNotNull($bgBanner) ? UrlHelper::getUrlFromFile($bgBanner) : '';
    $titleBanner = $this->getSettings('title');
    $descriptionBanner = $this->getSettings('description');
    $bannerType = $this->getSettings('banner_type', 'v1');
    $bannerUrl = $this->getSettings('banner_url');
    $bannerIsLink = AttributeHelper::stringNotNull($bannerUrl);
    $bannerTagOpen = $bannerIsLink ? "a href='$bannerUrl' alt='$titleBanner'" : 'div';
    $bannerTagClose = $bannerIsLink ? 'a' : 'div';
    $returnBanner = "";

    switch ($bannerType) {
      case "default":
        $returnBanner = "
                <$bannerTagOpen class='ps-banner ps-banner-v1'>
                    <div class='ps-banner-image'>
                        <img alt='$titleBanner' src='$bgImage'/>
                    </div>
                    <div class='ps-banner-text-container'>
                        <div class='ps-banner-text'>
                            <h3>$titleBanner</h3>
                            <p>$descriptionBanner</p>
                        </div>
                    </div>
                </$bannerTagClose>";
        break;
      case "v1":
        $returnBanner = "
                <$bannerTagOpen class='ps-banner ps-banner-v2'>
                    <div class='ps-banner-image'>
                        <img alt='$titleBanner' src='$bgImage'/>
                    </div>
                    <div class='ps-banner-text-container'>
                        <div class='ps-banner-text'>
                            <h3>$titleBanner</h3>
                            <p>$descriptionBanner</p>
                        </div>
                    </div>
                </$bannerTagClose>";
        break;
      case "v2":
        $returnBanner = "
                <$bannerTagOpen class='ps-banner ps-banner-v3'>
                    <div class='ps-banner-image'>
                        <img alt='$titleBanner' src='$bgImage'/>
                    </div>
                    <div class='ps-banner-text-container'>
                        <div class='ps-banner-text'>
                            <h3>$titleBanner</h3>
                            <p>$descriptionBanner</p>
                        </div>
                    </div>
                </$bannerTagClose>";
        break;
    }

    return $this->renderShortcode($returnBanner, TRUE);
  }

}

