<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_button",
 *   title = @Translation("Button"),
 *   description = @Translation("Create button."),
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "button_type",
 *         "name" = @Translation("Button type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "btn" = "Default",
 *              "btn-outline" = "Outline",
 *              "btn-texture" = "Texture"
 *         },
 *         "value" = "btn"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "button_size",
 *         "name" = @Translation("Button size"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "" = "Default",
 *              "btn-lg" = "Large",
 *              "btn-sm" = "Small",
 *         },
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "button_block",
 *         "name" = @Translation("Button width"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "" = "Default",
 *              "btn-block" = "Full width",
 *         },
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "button_color",
 *         "name" = @Translation("Button color"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "primary" = "Primary",
 *              "secondary" = "Secondary",
 *              "success" = "Success",
 *              "danger" = "Danger",
 *              "warning" = "Warning",
 *              "info" = "Info",
 *              "light" = "Light",
 *              "dark" = "Dark",
 *              "link" = "Link"
 *         },
 *         "value" = "primary"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "button_disabled",
 *         "name" = @Translation("Disabled"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "" = "Default",
 *              "ps-button-disabled" = "True",
 *              "" = "False",
 *         },
 *         "value" = ""
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "url",
 *         "name" = @Translation("Button url"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "title",
 *         "name" = @Translation("Button text"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "file",
 *         "atr_name" = "button_background",
 *         "name" = @Translation("Button background"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "icon",
 *         "atr_name" = "left_icon",
 *         "name" = @Translation("Select left button icon"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "icon",
 *         "atr_name" = "right_icon",
 *         "name" = @Translation("Select right button icon"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Button extends PsShortcodeBase {

  public function buildElement(): string {
    $buttonTitle = $this->getSettings('title');
    $buttonType = $this->getSettings('button_type');
    $buttonColor = $this->getSettings('button_color');
    $buttonSize = $this->getSettings('button_size');
    $buttonWidth = $this->getSettings('button_block');
    $buttonDisabled = $this->getSettings('button_disabled');
    $buttonIconRight = $this->getSettings('right_icon');
    $buttonIconLeft = $this->getSettings('left_icon');
    $buttonUrl = $this->getSettings('url');
    $bgButton = $this->getSettings('button_background');
    $bgImage = AttributeHelper::stringNotNull($bgButton) ? UrlHelper::getUrlFromFile($bgButton) : '';

    $bgButtonStyle = $buttonType === "btn-texture" ? "style='background-image: url($bgImage);'" : '';
    $buttonUrlTag = AttributeHelper::stringNotNull($buttonUrl) ? "href='{$this->getSettings('url')}'" : '';

    if (strlen($buttonIconRight) > 0) {
      $buttonIconRight = "<span class='ps-button-icon-right $buttonIconRight'></span>";
    }
    if (strlen($buttonIconLeft) > 0) {
      $buttonIconLeft = "<span class='ps-button-icon-left $buttonIconLeft'></span>";
    }

    $returnButton = "<a 
          $buttonUrlTag
          type='button' 
          $bgButtonStyle
          class='btn $buttonType-$buttonColor $buttonSize $buttonWidth $buttonDisabled'>
          $buttonIconLeft $buttonTitle $buttonIconRight
      </a>";

    return $this->renderShortcode($returnButton);
  }

}

