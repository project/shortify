<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_quote",
 *   title = @Translation("Quote"),
 *   description = @Translation("Create blockquote."),
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "quote_type",
 *         "name" = @Translation("Quote type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = "Default"
 *         },
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "textarea",
 *         "atr_name" = "quote_desc",
 *         "name" = @Translation("Quote"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "quote_author",
 *         "name" = @Translation("Quote author"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "quote_source",
 *         "name" = @Translation("Quote source title"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Blockquote extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $quoteType = $this->getSettings('quote_type', 'v1');
        $quoteDesc = $this->getSettings('quote_desc');
        $quoteAuthor = $this->getSettings('quote_author');
        $quoteSource = $this->getSettings('quote_source');
        $addFooter = AttributeHelper::stringNotNull($quoteAuthor) || AttributeHelper::stringNotNull($quoteSource);

        $footer = $addFooter ? "<footer>
                                    <div class='ps-quote-author'>$quoteAuthor</div><cite title='$quoteSource'>, $quoteSource</cite> 
                                </footer>" : "";

        $returnQuote = "";
        switch ($quoteType) {
            case "default":
                $returnQuote = "
                    <blockquote>
                        <i class='fas fa-quote-right'></i>
                        <p>$quoteDesc</p>
                        $footer
                    </blockquote>
                ";
                break;
        }


        return $this->renderShortcode($returnQuote);
    }
}

