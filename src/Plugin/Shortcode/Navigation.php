<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\Helpers\MenuHelper;
use Drupal\shortify\AdditionalClass\Helpers\UrlHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;
use Drupal\search\Form\SearchBlockForm;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_navigation",
 *   title = @Translation("Main navigation"),
 *   description = @Translation("Create a main page navigation"),
 *   group = "0",
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "name",
 *         "name" = @Translation("Menu type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *             "default" = @Translation("Default")
 *         },
 *         "value" = "default"
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "navigation",
 *         "name" = @Translation("Main menu"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "menu",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "file",
 *         "atr_name" = "logo_img",
 *         "name" = @Translation("Navigation logo"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "search_box",
 *         "name" = @Translation("Search in navigation"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "small_nav",
 *         "name" = @Translation("Smaller navigation"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *     {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Navigation extends PsShortcodeBase {

  public function getSearchForm(): string {
    $form = Drupal::formBuilder()->getForm(SearchBlockForm::class);
    $form['keys']['#attributes']['placeholder'] = t('Enter text to search...');
    $form['actions']['#attributes']['class'][0] .= ' ps-hide';
    $render = render($form);
    $searchButtonText = t('Search button');
    $render = str_replace('</form>', "<button title='$searchButtonText' type='reset' class='search ps-search-button'></button></form>", $render);
    return str_replace('class="form-search"', 'class="form-search ps-input-search"', $render);
  }

  public function buildElement(): string {
    $logoImg = $this->getSettings('logo_img');
    $navigation = $this->getSettings('navigation');
    $isSearchBox = AttributeHelper::isTrue($this->getSettings('search_box'));
    $isSmallNae = AttributeHelper::isTrue($this->getSettings('small_nav'));

    $navigationLogo = AttributeHelper::stringNotNull($logoImg) ? UrlHelper::getUrlFromFile($logoImg) : '';
    $links = MenuHelper::getMenu($navigation, TRUE, TRUE);
    $search_block = $isSearchBox ? '<li type="search"><div>' . $this->getSearchForm() . '</div></li>' : "";
    $smaller_nav = $isSmallNae ? "ps-navigation-small" : "";

    $render_navigation = "
      <div id='ps-main-navigation' class='$smaller_nav'>
          <div class='container'>
              <div class='ps-menu-logo'>
                  <a href='/'><img alt='Site logo' src='$navigationLogo'/></a>
              </div>
              <div class='ps-menu-links'>
                  <nav class='ps-menu-nav'>
                      <ul class='ps-nav ps-unstyled-list'>$links$search_block</ul>
                  </nav>
                  <div class='hamburger hamburger--squeeze' type='button'>
                    <span class='hamburger-box'>
                      <span class='hamburger-inner'></span>
                    </span>
                  </div>
              </div>
          </div>
      </div>";

    return $this->renderShortcode($render_navigation);
  }

}
