<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;
use Drupal\views\Views;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_viewrender",
 *   title = @Translation("View"),
 *   description = @Translation("Render selected view"),
 *   group = "2",
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "view_name",
 *         "name" = @Translation("Select view"),
 *         "width" = "50",
 *         "select_type" = "server",
 *         "select_server" = "view",
 *         "value" = "default"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class ViewRender extends PsShortcodeBase {

  public function buildElement(): string {
    $viewName = $this->getSettings('view_name');
    $renderView = "";

    if (AttributeHelper::stringNotNull($viewName)) {
      $id = explode(':', $viewName)[0];
      $display = explode(':', $viewName)[1] ?? NULL;

      $view = Views::getView($id);
      if (!is_null($display)) {
        $view->setDisplay($display);
        $view->preExecute();
        $view->execute();
      }
      $view = $view->buildRenderable();
      $renderView = Drupal::service('renderer')->renderRoot($view);
    }

    return $this->renderShortcode($renderView);
  }
}
