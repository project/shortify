<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_slider",
 *   title = @Translation("Slider"),
 *   description = @Translation("Create slider container."),
 *   dependency = {
 *      "parent" = {},
 *      "child" = { "ps_slider_item" }
 *   },
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "slide_effect",
 *         "name" = @Translation("Slide effect"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "slide" = @Translation("slide"),
 *              "fade" = @Translation("fade"),
 *              "cube" = @Translation("cube"),
 *              "coverflow" = @Translation("coverflow"),
 *              "flip" = @Translation("flip")
 *         },
 *         "value" = "slide"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "slider_height",
 *         "name" = @Translation("Slider height"),
 *         "width" = "25",
 *         "value" = "500px"
 *      },
 *     {
 *         "type" = "number",
 *         "atr_name" = "slide_per_view",
 *         "name" = @Translation("Slide per view"),
 *         "width" = "25",
 *         "value" = "1"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "have_scrollbar",
 *         "name" = @Translation("Use scrollbar?"),
 *         "width" = "25",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "have_arrows",
 *         "name" = @Translation("Use nav arrows?"),
 *         "width" = "25",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "have_pagination",
 *         "name" = @Translation("Show pagination dots?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *     {
 *         "type" = "select",
 *         "atr_name" = "slider_layout",
 *         "name" = @Translation("Slider direction"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "horizontal" = @Translation("horizontal"),
 *              "vertical" = @Translation("vertical")
 *         },
 *         "value" = "horizontal"
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_autoplay",
 *         "name" = @Translation("Autoplay slides?"),
 *         "width" = "25",
 *         "value" = "true"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "autoplay_time",
 *         "name" = @Translation("Autoplay time"),
 *         "width" = "25",
 *         "value" = "3000"
 *      },
 *   }
 * )
 */
class Slider extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $additionalScriptStyle = "
          <link rel=\"stylesheet\" href=\"https://unpkg.com/swiper@7/swiper-bundle.min.css\">
          <script src=\"https://unpkg.com/swiper@7/swiper-bundle.min.js\"></script>
        ";

        $height = $this->getSettings('slider_height', "500px");
        $autoplayTime = $this->getSettings('autoplay_time', '3000');
        $slidePerView = $this->getSettings('slide_per_view', '1');

        $layout = $this->getSettings('slider_layout');
        $slideEffect = $this->getSettings('slide_effect');

        $isAutoplay = AttributeHelper::isTrue($this->getSettings('is_autoplay'));
        $havePagination = AttributeHelper::isTrue($this->getSettings('have_pagination'));
        $haveArrows = AttributeHelper::isTrue($this->getSettings('have_arrows'));
        $haveScrollbar = AttributeHelper::isTrue($this->getSettings('have_scrollbar'));

        $pagination = $havePagination ? "pagination: {el: '.swiper-pagination',dynamicBullets: true,}," : "";
        $paginationTemplate = $havePagination ? "<div class='swiper-pagination'></div>" : "";
        $arrows = $haveArrows ? "navigation: {nextEl: '.swiper-button-next',prevEl: '.swiper-button-prev',}," : "";
        $arrowsTemplate = $haveArrows ? "<div class='swiper-button-prev'></div><div class='swiper-button-next'></div>" : "";
        $scrollbar = $haveScrollbar ? "scrollbar: {el: '.swiper-scrollbar',}," : "";
        $scrollbarTemplate = $haveScrollbar ? "<div class='swiper-scrollbar'></div>" : "";
        $autoplay = $isAutoplay ? "autoplay: {delay: {$autoplayTime},disableOnInteraction: true}" : "";

        $sliderInit = "
          <script>
            let maxInit = 20;
            const init = 0;
            function initSlider(){
              if(typeof Swiper === 'undefined'){
                maxInit++;
                if(init < maxInit){
                  setTimeout(function(){
                    initSlider();
                  },200);
                }else{
                  console.error('Not found Swiper lib');
                }
              }else{
                const slider = new Swiper('.swiper', {
                  direction: '$layout',
                  loop: true,
                  effect: '$slideEffect',
                  slidesPerView: $slidePerView,
                  $pagination
                  $arrows
                  $scrollbar
                  $autoplay
                })
              }
            }
            setTimeout(initSlider,500);
          </script>";

        return $this->renderShortcode("$additionalScriptStyle
          <div class='swiper' style='width: 100%; height: $height'>
              <div class='swiper-wrapper'>
                  {$this->getContent()}
              </div>
              $paginationTemplate
              $arrowsTemplate
              $scrollbarTemplate
          </div>
          $sliderInit
        ");
    }
}

