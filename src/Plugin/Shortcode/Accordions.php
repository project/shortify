<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_accordion",
 *   title = @Translation("Accordion item"),
 *   description = @Translation("Create accordion item."),
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "accordion_type",
 *         "name" = @Translation("Accordion type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = @Translation("Default"),
 *              "v1" = @Translation("Alternative 1"),
 *              "v2" = @Translation("Alternative 2"),
 *         },
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "text",
 *         "atr_name" = "title",
 *         "name" = @Translation("Accordion title"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "checkbox",
 *         "atr_name" = "is_opened",
 *         "name" = @Translation("Opened by default"),
 *         "width" = "25",
 *         "value" = "false"
 *      },
 *   }
 * )
 */
class Accordions extends PsShortcodeBase {

  public function buildElement(): string {
    $accordionTitle = $this->getSettings('title');
    $isDefaultOpened = AttributeHelper::isTrue($this->getSettings('is_opened'));
    $accordionOpenedClass = $isDefaultOpened ? 'ps-accordion-active ps-accordion-hover' : '';
    $accordionOpenedIcon = $isDefaultOpened ? 'fa-chevron-down ps-accordion-hover' : 'fa-chevron-right';
    $accordionOpenedBlockStyle = $isDefaultOpened ? "style='display: block;'" : '';

    $accordionType = $this->getSettings('accordion_type', 'v1');
    $returnAccordion = "";

    switch ($accordionType) {
      case "default":
        $returnAccordion = "
                <div class='ps-accordion ps-accordion-v1'>
                    <h4 class='$accordionOpenedClass'><i class='fas $accordionOpenedIcon'></i>$accordionTitle</h4>
                    <div class='ps-accordion-text' $accordionOpenedBlockStyle>
                        {$this->getContent()}
                    </div>
                </div>";
        break;
      case "v1":
        $returnAccordion = "
                <div class='ps-accordion ps-accordion-v2'>
                    <h4 class='$accordionOpenedClass'><i class='fas $accordionOpenedIcon'></i>$accordionTitle</h4>
                    <div class='ps-accordion-text' $accordionOpenedBlockStyle>
                        {$this->getContent()}
                    </div>
                </div>";
        break;
      case "v2":
        $returnAccordion = "
                <div class='ps-accordion ps-accordion-v3'>
                    <h4 class='$accordionOpenedClass'><i class='fas $accordionOpenedIcon'></i>$accordionTitle</h4>
                    <div class='ps-accordion-text' $accordionOpenedBlockStyle>
                        {$this->getContent()}
                    </div>
                </div>";
        break;
    }

    return $this->renderShortcode($returnAccordion, TRUE);
  }

}

