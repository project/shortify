<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic icon shortcode
 *
 * @Shortcode(
 *   id = "ps_counter",
 *   title = @Translation("Counter"),
 *   description = @Translation("Add animated counters."),
 *   settings = {
 *      {
 *         "type" = "select",
 *         "atr_name" = "counter_type",
 *         "name" = @Translation("A counter type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = @Translation("Default"),
 *              "v1" = @Translation("Alternative 1"),
 *              "v2" = @Translation("Alternative 2"),
 *              "v3" = @Translation("Alternative 3")
 *         },
 *         "value" = "default"
 *      },
 *     {
 *         "type" = "icon",
 *         "atr_name" = "icon_name",
 *         "name" = @Translation("Select icon to show in"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "counter_start",
 *         "name" = @Translation("Counter start number"),
 *         "width" = "25",
 *         "value" = "0"
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "counter_end",
 *         "name" = @Translation("Counter end number"),
 *         "width" = "25",
 *         "value" = "100"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "icon_size",
 *         "name" = @Translation("Icon size"),
 *         "width" = "25",
 *         "value" = "25"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "border_size",
 *         "name" = @Translation("Border size"),
 *         "width" = "25",
 *         "value" = "2"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "icon_color",
 *         "name" = @Translation("Icon color"),
 *         "width" = "25",
 *         "value" = "#ffffff"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "back_color",
 *         "name" = @Translation("Background color"),
 *         "width" = "25",
 *         "value" = "#000000"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "border_color",
 *         "name" = @Translation("Border color"),
 *         "width" = "25",
 *         "value" = "#000000"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "text_color",
 *         "name" = @Translation("Text color"),
 *         "width" = "25",
 *         "value" = "#ffffff"
 *      },
 *      {
 *         "type" = "html",
 *         "atr_name" = "content_text",
 *         "name" = @Translation("Content text"),
 *         "width" = "100",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Counter extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $type = $this->getSettings('counter_type');
        $icon = $this->getSettings('icon_name');
        $start = (float)$this->getSettings('counter_start', '0.0');
        $end = (float)$this->getSettings('counter_end', '100.0');
        $iconSize = (int)$this->getSettings('icon_size', '25');
        $iconColor = $this->getSettings('icon_color', "#000000");
        $borderSize = (int)$this->getSettings('border_size', '2');
        $backgroundColor = $this->getSettings('back_color', "#000000");
        $textColor = $this->getSettings('text_color', "#ffffff");
        $borderColor = $this->getSettings('border_color', "#000000");
        $htmlAfter = $this->getSettings('content_text');

        $iconHtml = AttributeHelper::stringNotNull($icon)
            ? "<span class='ps-counter-icon $icon' style='font-size: {$iconSize}px; color: $iconColor;'></span>"
            : '';

        $typeClass = '';

        switch ($type)
        {
          case "default":
            $typeClass = 'ps-counter-circle-full';
            break;
          case "v1":
            $typeClass = 'ps-counter-circle-bordered';
            break;
          case "v2":
            $typeClass = 'ps-counter-rectangle-full';
            break;
          case "v3":
            $typeClass = 'ps-counter-rectangle-bordered';
            break;
        }

        $html = "
          <div class='ps-counter $typeClass'>
            <div class='ps-counter-in' style='background: $backgroundColor; border-color: $borderColor; border-width: {$borderSize}px'>
              <div class='ps-counter-content'>
                $iconHtml
                <h4 class='ps-counter-number' style='color: $textColor' data-from='$start' data-to='$end' data-speed='2500'></h4>
              </div>
            </div>
            <div class='ps-counter-after'>
              $htmlAfter
            </div>
          </div>
        ";

        return $this->renderShortcode($html, true);
    }
}

