<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal;
use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_breadcrumbs",
 *   title = @Translation("Breadcrumbs"),
 *   description = @Translation("Create a breadcrumbs element"),
 *   group = "0",
 *   settings = {
 *     {
 *         "type" = "file",
 *         "atr_name" = "background_img",
 *         "name" = @Translation("Background image"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *     {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Breadcrumbs extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $routeMatch = Drupal::routeMatch();
        $pageTitle = Drupal::service('title_resolver')
            ->getTitle(Drupal::request(), $routeMatch->getRouteObject());
        $breadcrumb = Drupal::service('breadcrumb')
            ->build($routeMatch)
            ->toRenderable();
        $breadcrumbs = Drupal::service('renderer')->render($breadcrumb);
        $breadcrumbs = str_replace('<nav', '<nav class="ps-breadcrumb-nav"', $breadcrumbs);
        $bgBreadcrumbs = $this->getSettings('background_img');

        if (is_array($pageTitle)) $pageTitle = $pageTitle['#markup'];

        $html = "
                <div id='ps-breadcrumbs' style='background-image: url($bgBreadcrumbs);'>
                    <div class='container'>
                        " . $breadcrumbs . "
                        <div class='ps-page-title'><h1>$pageTitle</h1></div>
                    </div>
                </div>";

        return $this->renderShortcode($html);
    }
}

