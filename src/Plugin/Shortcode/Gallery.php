<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_gallery",
 *   title = @Translation("Gallery"),
 *   description = @Translation("Create gallery container."),
 *   dependency = {
 *      "parent" = {},
 *      "child" = { "ps_image", "ps_video"  }
 *   },
 *   settings = {
 *     {
 *         "type" = "number",
 *         "atr_name" = "gap",
 *         "name" = @Translation("Grid gap (px)"),
 *         "width" = "25",
 *         "value" = "0"
 *      },
 *     {
 *         "type" = "number",
 *         "atr_name" = "img_count",
 *         "name" = @Translation("How more img per row"),
 *         "width" = "25",
 *         "value" = "4"
 *      },
 *     {
 *         "type" = "number",
 *         "atr_name" = "row_height",
 *         "name" = @Translation("Grid row height (px)"),
 *         "width" = "25",
 *         "value" = "200"
 *      },
 *   }
 * )
 */
class Gallery extends PsShortcodeBase
{
    public function buildElement(): string
    {
        $rowHeight = $this->getSettings('row_height', '0') . "px";
        $gap = $this->getSettings('gap', '0') . "px";
        $imgCount = (int)$this->getSettings('img_count', '0');

        $gridGenerated = str_repeat("1fr ", $imgCount);

        $this->addDefStyle("display:grid; grid-template-columns: $gridGenerated; grid-auto-rows: $rowHeight; grid-column-gap: $gap; grid-row-gap: $gap;");
        $this->addDefClass('ps_image_gallery');

        return $this->renderShortcode($this->getContent(), true);
    }
}

