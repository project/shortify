<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_dropcap",
 *   title = @Translation("Dropcap"),
 *   description = @Translation("Create blockdropcap."),
 *   settings = {
 *     {
 *         "type" = "select",
 *         "atr_name" = "dropcap_type",
 *         "name" = @Translation("Dropcap type"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "default" = @Translation("Default"),
 *              "v2" = @Translation("Alternative 1"),
 *         },
 *         "value" = "default"
 *      }
 *   }
 * )
 */
class Dropcap extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $dropcapType = $this->getSettings('dropcap_type');
        $returnDropcap = "";

        switch ($dropcapType) {
            case "default":
                $returnDropcap = "
                    <div class='ps-dropcap-v1'>
                        {$this->getContent()}
                    </div>
                ";
                break;
            case "v2":
                $returnDropcap = "
                    <div class='ps-dropcap-v2'>
                        {$this->getContent()}
                    </div>
                ";
                break;
        }

        return $this->renderShortcode($returnDropcap);
    }
}

