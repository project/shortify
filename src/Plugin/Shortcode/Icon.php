<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\Helpers\AttributeHelper;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic icon shortcode
 *
 * @Shortcode(
 *   id = "ps_icon",
 *   title = @Translation("Icon"),
 *   description = @Translation("Enter a icon."),
 *   settings = {
 *     {
 *         "type" = "icon",
 *         "atr_name" = "icon_name",
 *         "name" = @Translation("Select icon"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "select",
 *         "atr_name" = "icon_position",
 *         "name" = @Translation("Icon position in container"),
 *         "width" = "50",
 *         "select_type" = "list",
 *         "select_list" = {
 *              "center" = @Translation("Center"),
 *              "top" = @Translation("Top"),
 *              "tr" = @Translation("Top-Right"),
 *              "right" = @Translation("Right"),
 *              "br" = @Translation("Bottom-Right"),
 *              "bottom" = @Translation("Bottom"),
 *              "bl" = @Translation("Bottom-Left"),
 *              "left" = @Translation("Left"),
 *              "tl" = @Translation("Top-Left")
 *         },
 *         "value" = "center"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "icon_size",
 *         "name" = @Translation("Icon size"),
 *         "width" = "25",
 *         "value" = "25"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "icon_color",
 *         "name" = @Translation("Icon color"),
 *         "width" = "25",
 *         "value" = "#000000"
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "icon_link",
 *         "name" = @Translation("Icon reference"),
 *         "width" = "50",
 *         "value" = ""
 *      },
 *      {
 *         "type" = "checkbox",
 *         "atr_name" = "is_back_container",
 *         "name" = @Translation("Use container with background?"),
 *         "width" = "50",
 *         "value" = "false"
 *      },
 *      {
 *         "type" = "text",
 *         "atr_name" = "back_size",
 *         "name" = @Translation("Back size (add %, px etc.)"),
 *         "width" = "50",
 *         "value" = "100%"
 *      },
 *      {
 *         "type" = "color",
 *         "atr_name" = "back_color",
 *         "name" = @Translation("Background color"),
 *         "width" = "50",
 *         "value" = "#000000"
 *      },
 *      {
 *         "type" = "number",
 *         "atr_name" = "back_round",
 *         "name" = @Translation("Round of background (%)"),
 *         "width" = "50",
 *         "value" = "100"
 *      },
 *      {
 *         "type" = "solo",
 *         "value" = "true"
 *      }
 *   }
 * )
 */
class Icon extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $html = t("Edit this, and select icon or remove this shortcode");
        $iconName = $this->getSettings('icon_name');
        $backColor = $this->getSettings('back_color', "#00000");
        $backSize = $this->getSettings('back_size', '100%');
        $backRound = $this->getSettings('back_round', '100');
        $isBackContainer = AttributeHelper::isTrue($this->getSettings('is_back_container'));
        $additionalSt = $isBackContainer ? "background: $backColor;" : '';

        if (AttributeHelper::stringNotNull($iconName)) {
            $iconPosition = $this->getSettings('icon_position');
            $iconSize = $this->getSettings('icon_size');
            $iconColor = $this->getSettings('icon_color');
            $iconLink = $this->getSettings('icon_link');

            $html = AttributeHelper::stringNotNull($iconLink)
                ? "<a href='$iconLink' class='link-with-icon icon-position icon-position-$iconPosition'><i class='$iconName ' style='font-size: {$iconSize}px; color:$iconColor'></i></a>"
                : "<i  class='$iconName icon-position icon-position-$iconPosition' style='font-size: {$iconSize}px; color:$iconColor;'></i>";
        }

        $this->addDefStyle("display:block !important; $additionalSt position: relative; margin: auto; width: $backSize !important; border-radius: $backRound%; padding-top:$backSize !important;");

        return $this->renderShortcode($html, true);
    }
}

