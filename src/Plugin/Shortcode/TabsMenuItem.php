<?php

namespace Drupal\shortify\Plugin\Shortcode;

use Drupal\shortcode\Annotation\Shortcode;
use Drupal\shortify\AdditionalClass\PsShortcodeBase;

/**
 * Provides a basic button shortcode
 *
 * @Shortcode(
 *   id = "ps_tabs_menu_item",
 *   title = @Translation("Tabs menu item"),
 *   description = @Translation("Create a tabs item to tabs menu."),
 *   dependency = {
 *      "parent" = { "ps_tabs_menu" },
 *      "child" = {}
 *   },
 *   settings = {
 *     {
 *         "type" = "text",
 *         "atr_name" = "tabs_item_name",
 *         "name" = @Translation("Tabs item name"),
 *         "width" = "25",
 *         "value" = "Simple tab"
 *      },
 *   }
 * )
 */
class TabsMenuItem extends PsShortcodeBase
{

    public function buildElement(): string
    {
        $itemName = $this->getSettings('tabs_item_name', "###");

        return $this->renderShortcode("
          <div class='tabs-menu-item-name'>$itemName</div>
          <div class='tabs-menu-item'>
                {$this->getContent()}
          </div>
        ");
    }
}

